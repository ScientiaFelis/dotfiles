"set termguicolors
set guicursor=n-v-c:hor75,i-ci-ve:ver25,r-cr:hor20,o:hor50,a:blinkwait700-blinkoff400-blinkon250-nCursor/lCursor,sm:block-blinkwait175-blinkoff150-blinkon175
"set guicursor= "Fix the change to clock cursor in terminal.

" Plugins and plugin settings
"{{{
"
" Load Plugins
call plug#begin('~/.vim/plugged')

Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'itchyny/lightline.vim'
Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'joshdick/onedark.vim'
Plug 'tpope/vim-surround'
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'jalvesaq/Nvim-R' | Plug 'vim-pandoc/vim-rmarkdown'
"Plug 'donRaphaco/neotex', { 'for': 'tex' }

call plug#end()

" Settings of the plugins
"
" vim-pandoc
let g:pandoc#syntax#conceal#blacklist = ["atx", "titleblock"] " Do not change the symbols for headings.

let g:pandoc#folding#fdc = 0 " Do not show windows foldcolumn.
let g:pandoc#folding#level = 3


"vim-neotex latex-preview
"let g:neotex_delay = 2000
"let g:tex_flavor = 'latex'


"}}}

" Setting options
"{{{
filetype plugin indent on
syntax on

" Set the spellfile to all.latin1.add
set spellfile=~/.vim/spell/all.latin1.add

set nocompatible " Use Vim rather than Vi settings

" Show the file name in the title bar
set title

" Highlights, current line and search
set cursorline

set nohlsearch
set incsearch
set showmatch

" Indentation
set autoindent

set autochdir " Set the working directory to current file directory

" History
set history=50
set undofile " Set capability of undo even after closed the file.

set ruler		" Show mouse position
set showcmd		" display inclomplete commands

" Softtabs, 4 spaces
set tabstop=4
set softtabstop=0 " What does this do?
set shiftwidth=4
set shiftround
set noexpandtab


" Use one space after puctuation instead of two
set nojoinspaces

" Show 80 characters
"set textwidth=80
"set colorcolumn=+1

" Set not error bells
set noerrorbells

" Don't reset cursor to start of line
set nostartofline


" Foldings

set foldmethod=marker
highlight Folded guibg=red guifg=blue
highlight FoldColumn guibg=white guifg=green


" Show Linenumbers
set number
set numberwidth=4

" Show relative line numbers
if exists("&relativenumber")
	set relativenumber
	au BufReadPost * set relativenumber
endif

set ignorecase
set smartcase "Does not show lowercase matches if the search is with upper case.

"}}}


" Mappings and Autocommands
"{{{

"" Mappings
let mapleader = ","

" Tab completion
 " will insert tab at beginning of line,
 " will use completion if not at beginning
set wildmode=list:longest,list:full
set complete=.,w,t

function! InsertTabWrapper()
	let col = col('.') - 1
	if !col || getline('.')[col - 1] !~ '\k'
		return "\<tab>"
	else
		return "\<c-p>"
	endif
endfunction
inoremap <Tab> <c-r>=InsertTabWrapper()<cr>

"Map jk to the esc. 
inoremap jk <esc> 
" Map Y to function as C and D, i.e. y$
noremap Y y$

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h


" Set the spellcheck and language 
inoremap <leader>se <Esc>:setlocal spell spelllang=en<Enter>
inoremap <leader>ss <Esc>:setlocal spell spelllang=sv<Enter>

" Make brackets arround words.
inoremap <F5> <Esc>bi[<Esc>ea]<Esc>
inoremap <leader>p <Esc>bi(<Esc>ea)<Esc>
inoremap <leader>c <Esc>Bi{<Esc>Ea}<Esc>


"" Autocomands

" Source after writing .vimrc
"autocmd BufWritePost .vimrc source ~/.vimrc
"autocmd BufWritePost init.vim source $MYVIMRC

" Make shortcut comands for editing eg. html.
inoremap <Space><Space> <Esc>/<++><Enter>"_c4l

"Autocommand in insertmode i html
autocmd FileType html inoremap <leader>i <em></em><Space><++><Esc>FeT>i
autocmd FileType html inoremap <leader>b <strong></strong><Space><++><Esc>FsT>i
autocmd Filetype html inoremap <leader>p <p></p><Space><++><Esc>FpT>i

" Autocommand in insertmode i bash
autocmd FileType sh inoremap <leader>i2 if [<Space>*<Space>]; then <Enter><++><Enter>else<Enter><++><Enter>fi<Esc>kkkkf*cl
autocmd FileType sh inoremap <leader>i if [<Space>*<Space>]; then <Enter><++><Enter>fi<Esc>kkf*cl
autocmd FileType sh inoremap <leader>t [<space>*<space>]<space><++><Esc>F*cl


" Autocommands in insertmode i Markdown 
autocmd FileType markdown inoremap <leader>b <Space><Esc>i**+**<Space><++><Esc>F*F+cl
autocmd FileType markdown inoremap <leader>i <Space><Esc>i*+*<Space><++><Esc>F*F+cl
autocmd FileType markdown inoremap <leader>k <Space><Esc>i***+***<Space><++><Esc>F*F+cl
autocmd FileType markdown inoremap <leader>h1 <Esc>I#<Space>
autocmd FileType markdown inoremap <leader>h2 <Esc>o<Enter><Esc>I##<Space>
autocmd FileType markdown inoremap <leader>h3 <Esc>o<Enter><Esc>I###<Space>[**+**]{style="color:#808000;"}<++><Esc>F*F+xi
autocmd FileType markdown inoremap <leader>h4 <Esc>o<Enter><Esc>I####<Space>[**+**]{style="color:#862821;"}<++><Esc>F*F+xi
autocmd FileType markdown inoremap <leader>h5 <Esc>o<Enter><Esc>I#####<Space>[**+**]{style="color:#52660d;"}<++><Esc>F*F+xi
autocmd FileType markdown inoremap <leader>f <Esc>i![+](<++>)<++><Esc>F]F+cl
autocmd FileType markdown inoremap <leader>w <Esc>i[+](<++>)<++><Esc>F]F+cl

autocmd FileType markdown nnoremap <leader>C <Esc>:!pandoc % -f markdown -H $HOME/.pandoc/templates/headersubtit.tex --template=IndRef.tex -t latex+smart -o %<.pdf && xreader %<.pdf

"Add one citation block.
autocmd FileType markdown inoremap <leader>r <Esc>i[@<++>]<++><Esc>F@lc4l
" Finish two citations when you only have the nameyear tag.
autocmd FileType markdown inoremap <leader>2r <Esc>F;bi[@<Esc>f;wi@<Esc>ea]
" Finish three citations when you only have the nameyear tag.
autocmd FileType markdown inoremap <leader>3r <Esc>F;F;bi[@<Esc>f;wi@<Esc>f;wi@<Esc>ea]
" Finish four citations when you only have the nameyear tag.
autocmd FileType markdown inoremap <leader>4r <Esc>F;F;F;bi[@<Esc>f;wi@<Esc>f;wi@<Esc>f;wi@<Esc>ea]


" Add a metadata block and referens section to articles.
" Metadata article
autocmd FileType markdown inoremap <leader>B <Esc>gg0O---<Enter>title:<Space><++><Enter>author:<Space><++><Enter>fontsize:<Space>12pt<Enter>geometry:<Enter><Tab>-<Space>right=1.5cm<Enter>-<Space>left=2cm<Enter><Esc>I---<Enter><++><Enter><Enter><Esc>11kf<c4l


" Metedata beamer press

autocmd FileType markdown inoremap <leader>P <Esc>gg0O---<Enter>title:<Space><++><Enter>author:<Space><++><Enter>fontsize:<Space>12pt<Enter>theme:<Space>Montpellier<Enter>themecolor:<Space>rose<Enter>themefont:<Space>structuresmallcapsserif<Enter><Esc>I---<Enter><++><Enter><Enter><Esc>11kf<c4l


" Reference secrtion.
autocmd FileType markdown inoremap <leader>R <Esc>GA<Enter><Enter>### References<Enter>\noindent<Enter>\vspace{-2em}<Enter>\setlength{\parindent}{-0.6cm}<Enter>\setlength{\leftskip}{0.6cm}<Enter><Esc>


"}}}

" Backgrounds and coloschemes
"{{{
" (Have to be after the plugin section if you
" installed colorschemes with the plugin manager.)
"
set background=dark

" Set the lightline (border at the bottom) colorscheme
"
"let g:lightline = {'colorscheme': 'nordisk'}
let g:lightline = {'colorscheme': 'onedark'}

set noshowmode " Dont show mode (NORMAL INSERT) as we already do that with lightline

" Set the colorscheme
"colorscheme solarized
"colorscheme nordisk
colorscheme onedark

"autocmd BufWinEnter,FileType markdown colorscheme onedark
autocmd BufWinEnter,FileType tex colorscheme solarized

"}}}
